from rest_framework import serializers
from .models import Student


class StudentSerializer(
    serializers.ModelSerializer):  # ModelSerializer doesnt require to create a create and update method like we did in normal serializer. Here it happens automatically
    # name = serializers.CharField(read_only=True)  # Read only for single field
    class Meta:
        model = Student
        fields = '__all__'
        # read_only_fields = ['name', 'roll']  # Read only for multiple fields
        extra_kwargs = {  # another read only method for multiple fields
            'name': {'read_only': True},
            'roll': {'read_only': True}
        }
